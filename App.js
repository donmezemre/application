import "react-native-gesture-handler";
import * as React from "react";

import {SafeAreaProvider} from "react-native-safe-area-context";

import {createStore, combineReducers} from "redux";
import {Provider} from "react-redux";

import modalsReducer from "./src/store/reducers/modals";
import Main from "./src/Main";

const rootReducer = combineReducers({
    modals: modalsReducer,
});

const store = createStore(rootReducer);

const App = () => {
    return (
        <SafeAreaProvider>
            <Provider store={store}>
                <Main />
            </Provider>
        </SafeAreaProvider>
    );
};

export default App;
