import React from "react";

import {createStackNavigator} from "@react-navigation/stack";

import DashboardScreen from "../components/DashboardScreen/DashboardScreen";
import KvkkScreen from "../components/KvkkScreen/KvkkScreen";
import HelpScreen from "../components/HelpScreen/HelpScreen";
import ContactScreen from "../components/ContactScreen/ContactScreen";

export const navigationRef = React.createRef();

export function navigate(name, params) {
    navigationRef.current?.navigate(name, params);
}

const Stack = createStackNavigator();

const Navigator = () => {
    return (
        <Stack.Navigator initialRouteName="Dashboard" screenOptions={{headerShown: false}}>
            <Stack.Screen name="Dashboard" component={DashboardScreen} />
            <Stack.Screen name="Kvkk" component={KvkkScreen} />
            <Stack.Screen name="Help" component={HelpScreen} />
            <Stack.Screen name="Contact" component={ContactScreen} />
        </Stack.Navigator>
    );
};

export default Navigator;
