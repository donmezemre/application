export const TOGGLE_MENU_MODAL = "TOGGLE_MENU_MODAL";
export const TOGGLE_QUESTION_MODAL = "TOGGLE_QUESTION_MODAL";

export const toggleMenuModal = () => {
    return {type: TOGGLE_MENU_MODAL};
};

export const toggleQuestionModal = (texts, extraData?) => {
    return {type: TOGGLE_QUESTION_MODAL, texts: texts, extraData: extraData};
};
