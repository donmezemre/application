import {TOGGLE_MENU_MODAL, TOGGLE_QUESTION_MODAL} from "../actions/modals";

const initialState = {
    isMenuModalVisible: false,
    questionModalInfo: {
        isVisible: false,
        text: "defaultText",
        cancelText: "defaultCancel",
        okayText: "defaultOkay",
        extraData: {},
    },
};

const modalsReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_MENU_MODAL:
            return {
                ...state,
                isMenuModalVisible: !state.isMenuModalVisible,
            };
        case TOGGLE_QUESTION_MODAL:
            const modalInfo = {
                isVisible: !state.questionModalInfo.isVisible,
                text: action.texts.text,
                cancelText: action.texts.cancelText,
                okayText: action.texts.okayText,
                extraData: action.extraData,
            };
            return {
                ...state,
                questionModalInfo: modalInfo,
            };
        default:
            return state;
    }
};

export default modalsReducer;
