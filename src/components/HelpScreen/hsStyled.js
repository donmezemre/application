import styled from "styled-components/native";
import {View, Text, FlatList} from "react-native";

import {fonts} from "../../utils/fonts";
import colors from "../../utils/colors";

export const Container = styled(View)`
    flex: 1;
    background-color: ${colors.white};
`;

export const HeaderContainer = styled(View)`
    justify-content: center;
    align-items: center;
    padding: 10px;
`;

export const HeaderText = styled(Text)`
    font-family: ${fonts.primaryBold};
    color: ${colors.gray};
    font-size: 20px;
`;

export const HelpDataList = styled(FlatList).attrs({
    contentContainerStyle: {
        paddingBottom: 100,
    },
})`
    flex: 1;
`;
