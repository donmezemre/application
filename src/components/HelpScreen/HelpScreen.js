import React from "react";

import {Container, HeaderContainer, HeaderText, HelpDataList} from "./hsStyled";
import {helpData} from "../../Dummy/Dummy";
import HelpCard from "../_defaultComponents/Cards/HelpCard/HelpCard";
import {Alert} from "react-native";

const HelpScreen = (props) => {
    const {} = props;

    const cardClickHandler = (index) => {
        const message = helpData[index].text;
        alert(message);
    };

    const renderCard = ({item, index}) => {
        return <HelpCard data={item} cardClickHandler={() => cardClickHandler(index)} />;
    };

    return (
        <Container>
            <HeaderContainer>
                <HeaderText>Yardım</HeaderText>
            </HeaderContainer>
            <HelpDataList
                data={helpData}
                renderItem={renderCard}
                keyExtractor={(item, index) => index.toString()}
            />
        </Container>
    );
};

export default HelpScreen;
