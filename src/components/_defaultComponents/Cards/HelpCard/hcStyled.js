import styled from "styled-components/native";
import {TouchableOpacity, View, Image, Text} from "react-native";
import {fonts} from "../../../../utils/fonts";
import colors from "../../../../utils/colors";
import Dash from "react-native-dash";

export const Container = styled(TouchableOpacity)`
    width: 90%;
    margin: 16px;
    margin-bottom: 5px;
    margin-top: 5px;
    padding-bottom: 2px;
`;

export const IconContainer = styled(View)`
    margin-left: 40px;
`;

export const Icon = styled(Image)``;

export const TextContainer = styled(View)`
    flex: 1;
`;

export const CardText = styled(Text)`
    font-family: ${fonts.primaryMedium};
    color: ${colors.gray};
`;

export const Seperator = styled(Dash)`
    margin-top: 6px;
    margin-bottom: 6px;
    width: 90%;
    height: 1px;
`;

export const InnerContainer = styled(View)`
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

export const OuterContainer = styled(View)`
    align-items: center;
    width: 100%;
`;
