import React from "react";

import {
    OuterContainer,
    Container,
    IconContainer,
    Icon,
    TextContainer,
    CardText,
    SeperatorContainer,
    Seperator,
    InnerContainer,
} from "./hcStyled";
import {images} from "../../../../utils/images";

const HelpCard = (props) => {
    const {data, cardClickHandler} = props;
    return (
        <OuterContainer>
            <Container onPress={cardClickHandler}>
                <InnerContainer>
                    <TextContainer>
                        <CardText>{data.text}</CardText>
                    </TextContainer>
                    <IconContainer>
                        <Icon source={images.rightArrow} />
                    </IconContainer>
                </InnerContainer>
            </Container>
            <Seperator />
        </OuterContainer>
    );
};

export default HelpCard;
