import React from "react";

import {
    ButtonBorder,
    ButtonContainer,
    IconContainer,
    Icon,
    TextContainer,
    ButtonText,
} from "./qbStyled";
import {images} from "../../../../utils/images";

const QRButton = (props) => {
    const {onPress} = props;

    return (
        <ButtonBorder>
            <ButtonContainer onPress={onPress}>
                <IconContainer>
                    <Icon source={images.qr} />
                </IconContainer>
                <TextContainer>
                    <ButtonText>QR</ButtonText>
                </TextContainer>
            </ButtonContainer>
        </ButtonBorder>
    );
};

export default QRButton;
