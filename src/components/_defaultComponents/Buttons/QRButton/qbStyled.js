import styled from "styled-components/native";
import {View, Image, TouchableOpacity, Text} from "react-native";
import metrics from "../../../../utils/metrics";
import colors from "../../../../utils/colors";
import {fonts} from "../../../../utils/fonts";

export const ButtonBorder = styled(View)`
    border-width: 1px;
    height: ${metrics.height / 3}px;
    width: ${metrics.height / 3}px;
    border-radius: ${metrics.height / 3 / 2}px;
    padding: ${metrics.height / 3 / 20}px;
    border-color: ${colors.white};
    border-style: dashed;
`;

export const ButtonContainer = styled(TouchableOpacity)`
    flex: 1;
    background-color: ${colors.white};
    border-radius: ${metrics.height / 3 / 2}px;
    justify-content: center;
    align-items: center;
`;

export const IconContainer = styled(View)`
    margin-top: 15px;
    width: 35%;
    height: 35%;
`;

export const Icon = styled(Image).attrs({
    resizeMode: "contain",
})`
    width: 100%;
    height: 100%;
`;

export const TextContainer = styled(View)`
    margin-top: 16px;
`;

export const ButtonText = styled(Text)`
    font-family: ${fonts.primaryBold};
    font-size: 22px;
    color: ${colors.black};
`;
