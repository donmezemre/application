import React from "react";
import {useDispatch} from "react-redux";

import {Container, IconContainer, Icon, MenuText} from "./mbStyled";
import {images} from "../../../../utils/images";

import {toggleMenuModal} from "../../../../store/actions/modals";

const MenuButton = (props) => {
    const {isVisible} = props;
    const dispatch = useDispatch();

    const pressHandler = () => {
        dispatch(toggleMenuModal());
    };

    return (
        <Container isVisible={isVisible} onPress={pressHandler}>
            <IconContainer>
                <Icon isVisible={isVisible} source={isVisible ? images.close : images.menu} />
            </IconContainer>
            <MenuText isVisible={isVisible}>MENÜ</MenuText>
        </Container>
    );
};

export default MenuButton;
