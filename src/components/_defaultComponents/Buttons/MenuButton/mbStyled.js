import styled from "styled-components/native";
import {TouchableOpacity, View, Image, Text} from "react-native";
import colors from "../../../../utils/colors";
import {fonts} from "../../../../utils/fonts";

export const Container = styled(TouchableOpacity)`
    height: 74px;
    width: 74px;
    border-radius: 37px;
    background-color: ${(props) => (props.isVisible ? colors.transparent : colors.green)};
    bottom: 16px;
    left: 16px;
    position: absolute;
    justify-content: center;
    align-items: center;
    box-shadow: 0px 6px 24px rgba(46, 73, 92, 0.2306);
    padding: 6px;
    z-index: 1000;
`;

export const IconContainer = styled(View)`
    /* width: 32px;
    height: 32px; */
`;

export const Icon = styled(Image).attrs({
    resizeMode: "contain",
})`
    tint-color: ${(props) => (props.isVisible ? colors.green : colors.white)};
    /* width: 100%;
    height: 100%; */
`;

export const MenuText = styled(Text)`
    font-size: 12px;
    font-family: ${fonts.primaryMedium};
    text-align: center;
    color: ${(props) => (props.isVisible ? colors.green : colors.white)};
`;
