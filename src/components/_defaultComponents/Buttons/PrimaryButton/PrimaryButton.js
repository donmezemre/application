import React from "react";

import {Container, ButtonText} from "./pbStyled";

const PrimaryButton = (props) => {
    const {onPress, text} = props;
    return (
        <Container onPress={onPress}>
            <ButtonText>{text}</ButtonText>
        </Container>
    );
};

export default PrimaryButton;
