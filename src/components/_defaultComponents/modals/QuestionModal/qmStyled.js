import styled from "styled-components/native";
import {View, Text, TouchableOpacity} from "react-native";
import Modal from "react-native-modal";

import metrics from "../../../../utils/metrics";
import colors from "../../../../utils/colors";
import {fonts} from "../../../../utils/fonts";

export const ModalContainer = styled(Modal)``;

export const Container = styled(View)`
    padding: ${24}px;
    align-items: center;
    justify-content: center;
    border-radius: 4px;
    background-color: ${colors.white};
    width: ${metrics.width - 36}px;
`;

export const TitleContainer = styled(View)`
    align-items: center;
    justify-content: center;
`;

export const Title = styled(Text)`
    font-family: ${fonts.primaryBold};
    font-size: 18px;
`;

export const TextContainer = styled(View)`
    margin-top: 10px;
    justify-content: center;
    align-items: center;
`;

export const QuestionText = styled(Text)`
    font-family: ${fonts.primaryMedium};
    font-size: 14px;
    color: ${colors.gray};
    text-align: center;
    line-height: 24px;
`;

export const ButtonContainer = styled(View)`
    flex-direction: row;
    margin-top: 24px;
`;

export const CancelButton = styled(TouchableOpacity)`
    flex: 1;
    justify-content: center;
    align-items: center;
    height: 40px;
    background-color: ${colors.red};
    margin-right: 10px;
    border-radius: 120px;
`;

export const OkayButton = styled(TouchableOpacity)`
    flex: 1;
    justify-content: center;
    align-items: center;
    height: 40px;
    background-color: ${colors.green};
    border-radius: 120px;
    margin-left: 10px;
`;

export const CancelText = styled(Text)`
    font-family: ${fonts.primaryMedium};
    font-size: 14px;
    color: ${colors.white};
`;

export const OkayText = styled(Text)`
    font-family: ${fonts.primaryMedium};
    font-size: 14px;
    color: ${colors.white};
`;
