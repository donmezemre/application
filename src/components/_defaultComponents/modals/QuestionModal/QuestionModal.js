import React from "react";
import {useDispatch} from "react-redux";

import {
    ModalContainer,
    Container,
    TitleContainer,
    Title,
    TextContainer,
    QuestionText,
    ButtonContainer,
    CancelButton,
    OkayButton,
    CancelText,
    OkayText,
} from "./qmStyled";

import {toggleQuestionModal} from "../../../../store/actions/modals";
import {Linking} from "react-native";

const QuestionModal = (props) => {
    const {info} = props;
    const dispatch = useDispatch();

    const closeModal = () => {
        dispatch(
            toggleQuestionModal({
                text: ``,
                cancelText: "",
                okayText: "",
                extraData: {},
            })
        );
    };

    const okayPressed = () => {
        Linking.openURL(`tel:${info.extraData.phoneNumber}`);
        closeModal();
    };

    return (
        <ModalContainer
            isVisible={info.isVisible}
            useNativeDriver={true}
            hideModalContentWhileAnimating={true}
        >
            <Container>
                <TitleContainer>
                    <Title>Uyarı</Title>
                </TitleContainer>
                <TextContainer>
                    <QuestionText>{info.text}</QuestionText>
                </TextContainer>
                <ButtonContainer>
                    <CancelButton onPress={closeModal}>
                        <CancelText>{info.cancelText}</CancelText>
                    </CancelButton>
                    <OkayButton onPress={okayPressed}>
                        <OkayText>{info.okayText}</OkayText>
                    </OkayButton>
                </ButtonContainer>
            </Container>
        </ModalContainer>
    );
};

export default QuestionModal;
