import React from "react";
import {Text, Linking, Platform, UIManager, LayoutAnimation} from "react-native";

import {useDispatch} from "react-redux";
import * as Navigation from "../../../../_core/Navigators";

import {
    ModalContainer,
    Container,
    MenuTextContainer,
    MenuText,
    WholeTextContainer,
    BlurredView,
} from "./mmStyled";

import {toggleMenuModal, toggleQuestionModal} from "../../../../store/actions/modals";

import {buttonData, phoneNumber} from "../../../../Dummy/Dummy";

const MenuModal = (props) => {
    const {isVisible} = props;
    const dispatch = useDispatch();

    onPress = (buttonIndex) => {
        buttonIndex !== 3 && dispatch(toggleMenuModal());
        switch (buttonIndex) {
            case 0:
                Navigation.navigate("Dashboard");
                break;
            case 1:
                Navigation.navigate("Help");
                break;
            case 2:
                Navigation.navigate("Kvkk");
                break;
            case 3:
                dispatch(
                    toggleQuestionModal(
                        {
                            text: `${phoneNumber} numaralı telefonu aramak istediğinize emin misiniz?`,
                            cancelText: "Geri Dön",
                            okayText: "Ara",
                        },
                        {phoneNumber}
                    )
                );
                break;
            default:
                break;
        }
    };

    return (
        isVisible && (
            <ModalContainer>
                <BlurredView />
                {buttonData.map((data, buttonIndex) => {
                    return (
                        <MenuTextContainer
                            key={buttonIndex}
                            onPress={() => onPress(buttonIndex)}
                            isLast={buttonData.length === buttonIndex + 1}
                        >
                            <MenuText>{data.title}</MenuText>
                        </MenuTextContainer>
                    );
                })}
            </ModalContainer>
        )
    );
};

export default MenuModal;
