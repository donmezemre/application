import styled from "styled-components/native";
import {View, SafeAreaView, Text, TouchableOpacity} from "react-native";
import {BlurView} from "@react-native-community/blur";

import colors from "../../../../utils/colors";
import {fonts} from "../../../../utils/fonts";

export const ModalContainer = styled(View)`
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    justify-content: center;
    align-items: center;
`;

export const MenuTextContainer = styled(TouchableOpacity)`
    margin-bottom: ${(props) => (props.isLast ? 0 : 24)}px;
`;

export const MenuText = styled(Text)`
    font-family: ${fonts.primaryBold};
    color: ${colors.green};
    font-size: 24px;
    text-align: left;
`;

export const BlurredView = styled(BlurView).attrs({
    blurType: "light",
    blurAmount: 32,
    reducedTransparencyFallbackColor: "white",
})`
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
`;
