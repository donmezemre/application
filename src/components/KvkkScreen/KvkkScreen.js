import React from "react";
import {View} from "react-native";

import {
    Container,
    HeaderContainer,
    HeaderText,
    KvkkText,
    KvkkDataScrollContainer,
} from "./ksStyled";

import {KvkkData} from "../../Dummy/Dummy";

const KvkkScreen = (props) => {
    const {} = props;
    return (
        <Container>
            <HeaderContainer>
                <HeaderText>Web Görünümü</HeaderText>
            </HeaderContainer>
            <KvkkDataScrollContainer>
                <KvkkText>{KvkkData}</KvkkText>
            </KvkkDataScrollContainer>
        </Container>
    );
};

export default KvkkScreen;
