import styled from "styled-components/native";
import {View, Image, TouchableOpacity, Text} from "react-native";
import colors from "../../utils/colors";
import metrics from "../../utils/metrics";
import {fonts} from "../../utils/fonts";

export const Container = styled(View)`
    flex: 1;
    padding-top: 16px;
    background-color: white;
`;

export const QrButtonContainer = styled(View)`
    height: 60%;
    background-color: ${colors.yellow};
    border-radius: 12px;
    margin-left: 16px;
    margin-right: 16px;
`;

export const HeaderContainer = styled(View)`
    justify-content: center;
    align-items: center;
    flex-direction: row;
    background-color: ${colors.yellow};
    border-top-left-radius: 12px;
`;

export const BottomContainer = styled(View)`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const LogoContainer = styled(View)`
    height: 50px;
    border-bottom-left-radius: 12px;
    border-bottom-right-radius: 12px;
    width: 70%;
    padding-top: 6px;
    padding-bottom: 6px;
    background-color: ${colors.white};
    justify-content: center;
    align-items: center;
`;

export const Logo = styled(Image).attrs({
    resizeMode: "contain",
})`
    width: 70%;
    height: 100%;
`;

export const BoxContainer = styled(View)`
    flex: 1;
`;

export const LeftYellowBox = styled(View)`
    flex: 1;
    padding-top: 6px;
    padding-bottom: 6px;
    background-color: ${colors.yellow};
    justify-content: center;
    align-items: center;
    height: 100%;
    border-top-left-radius: 12px;
    border-top-right-radius: 12px;
`;

export const WhiteBack = styled(View)`
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: ${colors.white};
    z-index: -1;
`;

export const RightYellowBox = styled(View)`
    height: 100%;
    flex: 1;
    padding-top: 6px;
    padding-bottom: 6px;
    background-color: ${colors.yellow};
    justify-content: center;
    align-items: center;
    border-top-left-radius: 12px;
    border-top-right-radius: 12px;
`;

export const Round = styled(View)`
    height: ${metrics.height / 3}px;
    width: ${metrics.height / 3}px;
    border-radius: ${metrics.height / 3 / 2}px;
    overflow: hidden;
`;

export const CloseButtonContainer = styled(TouchableOpacity)`
    position: absolute;
    bottom: 16px;
    left: 16px;
`;

export const CloseText = styled(Text)`
    font-family: ${fonts.primaryBold};
`;
