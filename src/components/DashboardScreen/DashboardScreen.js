"use strict";
import React, {useState} from "react";
import {View} from "react-native";
import {useNavigation} from "@react-navigation/native";
import {useDispatch} from "react-redux";

import {
    Container,
    QrButtonContainer,
    HeaderContainer,
    BottomContainer,
    LogoContainer,
    Logo,
    LeftYellowBox,
    RightYellowBox,
    WhiteBack,
    BoxContainer,
    Round,
    CloseButtonContainer,
    CloseText,
} from "./dsStyled";
import PrimaryButton from "../_defaultComponents/Buttons/PrimaryButton/PrimaryButton";
import {toggleMenuModal} from "../../store/actions/modals";
import QRButton from "../_defaultComponents/Buttons/QRButton/QRButton";
import {images} from "../../utils/images";
import {RNCamera} from "react-native-camera";

const DashboardScreen = (props) => {
    const {} = props;
    const [isCameraOpen, setIsCameraOpen] = useState(false);

    const dispatch = useDispatch();

    const qrButtonPressedHandler = () => {
        setIsCameraOpen((b) => !b);
    };

    return (
        <Container>
            <QrButtonContainer>
                <HeaderContainer>
                    <BoxContainer>
                        <LeftYellowBox />
                        <WhiteBack />
                    </BoxContainer>
                    <LogoContainer>
                        <Logo source={images.logo} />
                    </LogoContainer>
                    <BoxContainer>
                        <RightYellowBox />
                        <WhiteBack />
                    </BoxContainer>
                </HeaderContainer>
                <BottomContainer>
                    {isCameraOpen ? (
                        <Round>
                            <RNCamera
                                style={{flex: 1, borderRadius: 120}}
                                type={RNCamera.Constants.Type.back}
                                flashMode={RNCamera.Constants.FlashMode.on}
                                androidCameraPermissionOptions={{
                                    title: "Permission to use camera",
                                    message: "Kamerayı kullanmamız gerek!",
                                    buttonPositive: "Ok",
                                    buttonNegative: "Cancel",
                                }}
                            />
                        </Round>
                    ) : (
                        <QRButton onPress={qrButtonPressedHandler} />
                    )}
                    {isCameraOpen && (
                        <CloseButtonContainer onPress={qrButtonPressedHandler}>
                            <CloseText>Kapat</CloseText>
                        </CloseButtonContainer>
                    )}
                </BottomContainer>
            </QrButtonContainer>
        </Container>
    );
};

export default DashboardScreen;
