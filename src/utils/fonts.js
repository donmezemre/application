export const fonts = {
    primary: "Poppins-Regular",
    primaryLight: "Poppins-Light",
    primaryMedium: "Poppins-Medium",
    primaryBold: "Poppins-Bold",
};
