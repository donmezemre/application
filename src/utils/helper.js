import metrics from "./metrics";
import {PixelRatio, Platform, StatusBar} from "react-native";

const FIGMA_SCREEN_HEIGHT = 812;
const FIGMA_SCREEN_WIDTH = 375;

const helper = {
    getResponsiveHeight: (height) => {
        return height * (metrics.height / FIGMA_SCREEN_HEIGHT);
    },
    getResponsiveWidth: (width) => {
        return width * (metrics.width / FIGMA_SCREEN_WIDTH);
    },
    isIphoneX: () => {
        return (
            Platform.OS === "ios" &&
            !Platform.isPad &&
            !Platform.isTVOS &&
            (metrics.height === 812 ||
                metrics.width === 812 ||
                metrics.height === 896 ||
                metrics.width === 896)
        );
    },
    // guideline height for standard 5" device screen is 680
    getResponsiveFontSize: (fontSize, standardScreenHeight = 680) => {
        const heightPercent = (fontSize * deviceHeight) / standardScreenHeight;
        return Math.round(heightPercent);
    },
};

const {height, width} = metrics;
const standardLength = width > height ? width : height;
const offset = width > height ? 0 : Platform.OS === "ios" ? 78 : StatusBar.currentHeight; // iPhone X style SafeAreaView size in portrait

const deviceHeight =
    helper.isIphoneX() || Platform.OS === "android" ? standardLength - offset : standardLength;

export default helper;
