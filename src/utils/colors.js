export default {
    transparent: "rgba(255,255,255,0)",
    green: "#27A12A",
    white: "#FFFFFF",
    gray: "#3D3D3D",
    red: "#E57373",
    yellow: "rgb(229, 180,29)",
    black: "rgb(61,61,61)",
};
