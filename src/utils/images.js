import logo from "../assets/logo/logo.png";

import menu from "../assets/icons/menu.png";
import okay from "../assets/icons/okay.png";
import close from "../assets/icons/close.png";
import rightArrow from "../assets/icons/right-arrow.png";
import qr from "../assets/icons/QR.png";

export const images = {
    logo,
    menu,
    okay,
    close,
    rightArrow,
    qr,
};
