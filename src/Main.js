import React, {useEffect} from "react";
import {LogBox} from "react-native";
import styled from "styled-components/native";
import {SafeAreaView} from "react-native-safe-area-context";

import {navigationRef} from "./_core/Navigators";
import {NavigationContainer} from "@react-navigation/native";
import {useSelector} from "react-redux";

import Navigator from "./_core/Navigators";
import MenuModal from "./components/_defaultComponents/Modals/MenuModal/MenuModal";
import MenuButton from "./components/_defaultComponents/Buttons/MenuButton/MenuButton";
import QuestionModal from "./components/_defaultComponents/Modals/QuestionModal/QuestionModal";

LogBox.ignoreLogs(["Remote debugger"]);

const Wrapper = styled(SafeAreaView)`
    flex: 1;
`;

const Main = () => {
    const reduxModalStates = useSelector((state) => state.modals);

    return (
        <NavigationContainer ref={navigationRef}>
            <Wrapper mode="margin">
                <Navigator />
            </Wrapper>
            <MenuButton isVisible={reduxModalStates.isMenuModalVisible} />
            <MenuModal isVisible={reduxModalStates.isMenuModalVisible} />
            <QuestionModal info={reduxModalStates.questionModalInfo} />
        </NavigationContainer>
    );
};

export default Main;
