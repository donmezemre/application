module.exports = {
  bracketSpacing: false,
  jsxBracketSameLine: false,
  singleQuote: false,
  tabWidth: 4,
  printWidth: 100,
};
